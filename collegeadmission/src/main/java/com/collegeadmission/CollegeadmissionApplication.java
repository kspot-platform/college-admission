package com.collegeadmission;

import com.collegeadmission.model.StudentInfo;
import com.collegeadmission.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class CollegeadmissionApplication implements CommandLineRunner {

	public static void main(String[] args) {

		SpringApplication.run(CollegeadmissionApplication.class, args);
	}
	@Autowired
	private IStudentService studentService;

	@Override
	public void run(String... args) throws Exception {
//		System.out.println("Students added");
//		StudentInfo studentInfo1=new StudentInfo(1,"Jaydeep","9518587532",87.60,"514235471661","ADCET");
//		StudentInfo studentInfo2=new StudentInfo(2,"Abhishek","9518587431",70,"514231871661","RIT");
//		StudentInfo studentInfo3=new StudentInfo(3,"Hemant","7219128811",98,"514341871661","ADCET");
//		StudentInfo studentInfo4=new StudentInfo(4,"Akash","9665312670",84,"5142566871691","SKN");
//		studentService.addStudent(studentInfo1);
//		studentService.addStudent(studentInfo2);
//		studentService.addStudent(studentInfo3);
//		studentService.addStudent(studentInfo4);
// 		studentService.deleteStudent(4);
//		System.out.println("student deleted");
//		studentService.updateStudent(new StudentInfo(4,"Akash","9665312670",84,"5142566871691","SKN"));
//		System.out.println("update");
//	}
//		System.out.println("By name");
//		studentService.getByName("Jaydeep").forEach(System.out::println);
//		System.out.println("get by id");
//		Optional<StudentInfo> studentInfo=studentService.getById(2);
//		System.out.println("Student get by id"+studentInfo);

//		System.out.println("get By college name");
//		List<StudentInfo> studentInfo= studentService.getByCollegeName("SKN");
//		System.out.println("By college Name"+studentInfo);
		studentService.findByGreaterThanOverallPercentage(78).forEach(System.out::println);
	}
}