package com.collegeadmission.repository;

import com.collegeadmission.model.StudentInfo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface StudentRepository extends MongoRepository<StudentInfo,Integer> {
    List<StudentInfo>findByName(String name);
    List<StudentInfo>findByCollegeName(String collegeName);

    @Query("{overAllPercentage:{$gt:?0}}")
    List<StudentInfo> findByGreaterThanOverallPercentage(double overAllPercentage);
}
