package com.collegeadmission.exceptions;

public class StudentNotFoundExceptions extends Exception{
    public StudentNotFoundExceptions() {
    }

    public StudentNotFoundExceptions(String errorMessage) {
        super(errorMessage);
    }
}

