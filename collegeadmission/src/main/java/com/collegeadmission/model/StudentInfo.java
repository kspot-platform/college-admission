package com.collegeadmission.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StudentInfo {
    @Id
    private int studentId;
    private String name;
    private String mobileNumber;
    private double overAllPercentage;
    private String adhCardNumber;
    private String collegeName;
}