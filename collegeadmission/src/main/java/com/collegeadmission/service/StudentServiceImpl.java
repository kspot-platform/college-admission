package com.collegeadmission.service;

import com.collegeadmission.exceptions.StudentNotFoundExceptions;
import com.collegeadmission.model.StudentInfo;
import com.collegeadmission.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements IStudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public void addStudent(StudentInfo studentInfo) {
        studentRepository.insert(studentInfo);
    }

    @Override
    public void updateStudent(StudentInfo studentInfo) {
        studentRepository.save(studentInfo);
    }

    @Override
    public void deleteStudent(int studentId) {
        studentRepository.deleteById(studentId);
    }

    @Override
    public Optional<StudentInfo> getById(int studentId) throws StudentNotFoundExceptions {
       return Optional.ofNullable(studentRepository.findById(studentId).orElseThrow(()
               -> new StudentNotFoundExceptions("Id Not found")));
    }

    @Override
    public List<StudentInfo> getByName(String name) throws StudentNotFoundExceptions {
        List<StudentInfo>studentInfo=studentRepository.findByName(name);
        if(studentInfo.isEmpty()){
            throw new StudentNotFoundExceptions("No name found");
        }
        return studentInfo ;
    }

    @Override
    public List<StudentInfo> findByGreaterThanOverallPercentage(double overAllPercentage) throws StudentNotFoundExceptions {
        List<StudentInfo>studentInfo=studentRepository.findByGreaterThanOverallPercentage(overAllPercentage);
        List<StudentInfo>result=studentInfo.stream().filter(e->e.getOverAllPercentage()>overAllPercentage).collect(Collectors.toList());
        if(result.isEmpty()){
            throw new StudentNotFoundExceptions("No student found");
        }
        return result;
    }

    @Override
    public List<StudentInfo> getByCollegeName(String collegeName) throws StudentNotFoundExceptions {
        List<StudentInfo>studentInfo=studentRepository.findByCollegeName(collegeName);
        if(studentInfo.isEmpty()){
            throw new StudentNotFoundExceptions("No student found from"+collegeName);
       }
        return studentInfo;
    }
}
