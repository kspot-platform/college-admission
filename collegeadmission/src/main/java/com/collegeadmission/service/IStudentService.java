package com.collegeadmission.service;

import com.collegeadmission.exceptions.StudentNotFoundExceptions;
import com.collegeadmission.model.StudentInfo;

import java.util.List;
import java.util.Optional;

public interface IStudentService {
    public void addStudent(StudentInfo studentInfo);
    public void updateStudent(StudentInfo studentInfo);
    public void deleteStudent(int studentId);
    Optional<StudentInfo> getById(int studentId) throws StudentNotFoundExceptions;
    List<StudentInfo>getByName(String name) throws StudentNotFoundExceptions;

    List<StudentInfo> findByGreaterThanOverallPercentage(double overAllPercentage) throws StudentNotFoundExceptions;

    //    List<StudentInfo> getByGreaterThanOverallPercentage(double overAllPercentage) throws StudentNotFoundExceptions;
    List<StudentInfo>getByCollegeName(String collegeName) throws StudentNotFoundExceptions;
}
